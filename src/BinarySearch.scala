class BinarySearch {
  def search(element: Int, list: List[Int], firstIndex: Int, lastIndex: Int): Int = {
    val split: Int = (firstIndex + lastIndex) / 2
    element compare list(split) match {
      case 0 => split
      case 1 => search(element, list, split, lastIndex)
      case -1 => search(element, list, firstIndex, split)
    }
  }

  def search(element: Int, list: List[Int]): Int = {
    try search(element, list, 0, list.size) catch {
      case e: StackOverflowError => -1
    }
  }
}

object BinarySearch extends App {
  override def main(args: Array[String]): Unit = {
    val binarySearch: BinarySearch = new BinarySearch()
    println(binarySearch.search(7, List(1, 2, 3, 5, 6, 7)))
  }
}