

object FizzBuzz extends App {
  def fizzbuzz(number: Int): String = {
    (number % 3, number % 5) match {
      case (0, 0) => "fizzbuzz"
      case (0, _) => "fizz"
      case (_, 0) => "buzz"
      case _ => number.toString
    }
  }

  override def main(args: Array[String]) = {
    (1 to 100).foreach(fizzbuzz _ andThen println _)
  }

}